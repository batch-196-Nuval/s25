console.log("Thanks Lord!");

// JSON - JavaScript Object Notation
// a popular data format for applications to communicate with each other
// JSON may look like a JS Object, but it is actually a string

/*
	JSON Syntax:

	{
		"key1": "value",
		"key2": "value"
	}

	keys are wrapped in curly brace
*/

let sample1 = `
	{	
		"name": "Katniss Everdeen",
		"age": 20,
		"address": {
			"city": "Kansas City",
			"state": "Kansas"
		}
	}
`;

console.log(sample1);

// are we also able to turn a JSON into a JS Object? - yes
// JSON.parse() - will return the JSON as a JS Object

console.log(JSON.parse(sample1));


// JSON Array
// an array of JSON

let sampleArr = `

	[
		{

			"email": "jasonNewsted@gmail.com",
			"password": "iplaybass1234",
			"isAdmin": false
		},
		{
			"email": "larsDrums@gmail.com",
			"password": "metallicaMe80",
			"isAdmin": true
		}
	]

`;

console.log(sampleArr);

// can we use array methods on a JSON array? - no because JSON is a string
// so what can we do to be able to add more items / objects into our sampleArr? = JSON.parse()
// Parsed the JSON array to a JS array and saved it in a variable
let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr);

// can we now delete the last item in the JSON array? Yes

console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);

// If for example, we need to send this back to the client / front end, it should be in JSON format
// We can actually turn a JS Object into a JSON
// JSON.stringify() - will stringify JS Objects as JSON


sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);

// Database (JSON) => Server / API (JSON to JS Object to process) => sent as JSON => Client

// Mini-Activity

// Given a JSON Array, process it and convert it to a JS Object so we can manipulate the array
// delete the last item in the Array and add a new item in the Array
// Stringify the array back into JSON
// and update jsonArr with the stringified array

console.log("Mini-Activity");

let jsonArr = `
	[
		"pizza",
		"hamburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"
	]
`;

console.log(jsonArr);

let parsedjsonArr = JSON.parse(jsonArr);
console.log(parsedjsonArr);

console.log(parsedjsonArr.pop());
console.log(parsedjsonArr.push(`BBQ`));
console.log(parsedjsonArr);

jsonArr = JSON.stringify(parsedjsonArr);
console.log(jsonArr);


